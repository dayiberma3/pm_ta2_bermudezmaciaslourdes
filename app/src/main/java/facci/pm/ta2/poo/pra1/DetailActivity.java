package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    TextView precio, descripcion,nombre;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        nombre = (TextView) findViewById(R.id.nombre);
        descripcion = (TextView) findViewById(R.id.descripcion);
        precio = (TextView) findViewById(R.id.precio);
        img = (ImageView) findViewById(R.id.thumbnail);


        // INICIO - CODE6
        // definimos las linea de acceo al object id
        final DataQuery query = DataQuery.get("item");
        String parametro = getIntent().getExtras().getString("dato");
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null){

                    //llamamos los dato etablecidos en la app
                    String Precio = (String) object.get("price");
                    String Descripcion = (String) object.get("description");
                    String Nombre = (String) object.get("name");
                    Bitmap bitmap =(Bitmap) object.get("image");

                    //mostramos tras la convimacion de variable

                    precio.setText(Precio);
                    descripcion.setText(Descripcion);
                    nombre.setText(Nombre);
                    img.setImageBitmap(bitmap);
                }else{
                    //error
                }

            }
        });



        // FIN - CODE6

    }

}
